﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">Base.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../Base.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.2</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"DZ5F.31QU+!!.-6E.$4%*76Q!!&amp;-A!!!1R!!!!)!!!&amp;+A!!!!=!!!!!AJ#98.F,GRW&lt;'FC$%*B=W5O&lt;(:D&lt;'&amp;T=Q!!!+!8!)!!!$!!!#A!!!!!!!!%!!-!0!#]!"^!A!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!.K8/X#0^AUCZ?`M@A_Z&lt;/!!!!!Q!!!!1!!!!!-J4,=D7==Z'F13547/3&amp;AT5(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!$4`'G"XM7"3)-KX6%+4T/D!1!!!0`````5(9T:DQ#S"/G!#:DM_%*_!!!!%%Y`#`DS[K"759$'%Y_)O#=!!!!%!!!!!!!!!#=!!5R71U-!!!!"!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!-!!!!!!A!"!!!!!!!H!!!!+HC=9_"G9'JAO-!!R)R!T.4!^!()"O%,$!)=%"*#!Q$P3AM&lt;!!!!!%A!!!%E?*RD9-!%`Y%!3$%S-$!^!.)M;/*A'M;G*M"F,C[\I/,-5$?S9KJH".*-?Y!-*J![K$A02)\J#B#@1$?4$9M^!&amp;6I,%%!!!!-!!&amp;73524!!!!!!!$!!!"QA!!!TRYH,P!S-#1;7RBJM$%Q-!-:)MT.$!EZ[?E=D%!_1Q1I!*D5!!#I/:JI9E&lt;(DC="A2[`0)N9(\T'ZZO&amp;R7"ZBI6#;:3E7Y@&amp;:&amp;/(R774B;6&amp;X`_```@@)4H=,&gt;(TH&amp;('Z$;&lt;A[A_(%8&amp;1Y1"UCTA/D`A2EA64$T!I#G=425+$/5M"A?C$J]P-'%%7)RT-AI60P$QU"W!D8R&gt;(&lt;I!"X=$82)ZQY$%%P17JR`SK&amp;3V^Z!FMY*),H?=J&lt;/$7!J4R:L@R&lt;_+1&gt;,&gt;8O^7#!;?`W!$,#E%YOV'V$S!0_W%TN!FP/\(HT.Q&lt;`NU'NG`GU(Y_,2`=]"&gt;)$!Q9=MX9U;1(\P2"!*&amp;/,J$/'1//\#I3-'Z$/?[!2[NZ-(&amp;CI==0_'A1QI52(I.!'&amp;$!P)0L#;&lt;L&lt;D$BLA=()1A6!:%+I#1B7!K"VA&amp;RTBC$M-DZ_VL_`N!M5&lt;'V+=/1"R!R#$UAQSVG.A:!":S!1E;[&amp;K&lt;9"M*KA9,/Z"&lt;!&gt;I.'AA[5FB2*A0UA/3E9'+A&gt;B+5(9$V$UAM6SAW!1IOQ4)4I#SKY&amp;M!59)OQH%BIJX!NE"50%_+"PE&gt;A9'X,3TPYML=JK&amp;Z1=!+"K-QA!!!!!!$"=!A"%!!!1R.SYQ!!!!!!Q8!)!!!!!%-4=O-!!!!!!-&amp;Q#!%1!!"$%X,D!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Q8!)!2!!!%-4=O-!!!!!!5!1!!!'_HLP#89THTG(J4^*=I'S9!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!9!!"A:A!!99'!!'9!9!"I!"!!&lt;!!Q!'M!U!"IQ\!!;$V1!'A+M!"I$6!!;!KQ!'A.5!"I#L!!;!V1!'9+Y!"BD9!!9'Y!!'!9!!"`````Q!!"!$```````````````````````````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!^01!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!^MI?SMDU!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!^MI&gt;C9G*CML)^!!!!!!!!!!!!!!!!!!!!!0``!!!^MI&gt;C9G*C9G*C9L+S01!!!!!!!!!!!!!!!!!!``]!MI&gt;C9G*C9G*C9G*C9G+SMA!!!!!!!!!!!!!!!!$``Q#(BW*C9G*C9G*C9G*C9P[S!!!!!!!!!!!!!!!!!0``!)?SMI&gt;C9G*C9G*C9P\_`I=!!!!!!!!!!!!!!!!!``]!B\+SML+(9G*C9P\_`P\_BQ!!!!!!!!!!!!!!!!$``Q#(ML+SML+SB\,_`P\_`P[(!!!!!!!!!!!!!!!!!0``!)?SML+SML+S`P\_`P\_`I=!!!!!!!!!!!!!!!!!``]!B\+SML+SML,_`P\_`P\_BQ!!!!!!!!!!!!!!!!$``Q#(ML+SML+SMP\_`P\_`P[(!!!!!!!!!!!!!!!!!0``!)?SML+SML+S`P\_`P\_`I=!!!!!!!!!!!!!!!!!``]!B\+SML+SML,_`P\_`P\_BQ!!!!!!!!!!!!!!!!$``Q#SML+SML+SMP\_`P\_`L+S!!!!!!!!!!!!!!!!!0``!!#(B\+SML+S`P\_`L+SBQ!!!!!!!!!!!!!!!!!!``]!!!!!B\+SML,_`L+SBQ!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!)?SML+S9A!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!#(9A!!!!!!!!!!!!!!!!!!!!!!!!!!````````````````````````````````````````````!!!!!A!"!!!!!!!-!!&amp;'5%B1!!!!!!!$!!!'?!!!&amp;G&amp;YH/698UR46RD`4LHA,7"[CV4N5.PCB2EHSH3)/H1#&amp;Z8"`)=Y&gt;6)N&amp;%4(+)(C&gt;'\KNI\%"V^G"ME3ETU1YJ)F7U,-8HQQ#`(F0MS^&lt;&amp;'86-WSF\E`$T-;^8,XH8.\?_`N,3UCPLD&lt;=(*IP``@\`T/VQ,-&lt;R-7/C&lt;BN!*%O)_&lt;:A8SQT)"G+DA)@'5$Y(13BY$+@93"4&lt;TL=)NRS2:J%"B7#\H+[6T]!^+K\@6T_%,1I1\+*IH?.&amp;9PA+OM,T!`;99&amp;]2P&amp;YHH=H7L&lt;FAMH#?4DHWC\Q&amp;`0B:"BR"&lt;2F&gt;X":E%)A5Y,F;[-^4&lt;'20JO]Y+XMN-/B51*(FOPRB`'3WC[R_932+%52,241+;8!&lt;DY_/'EFN4+G&gt;BL+-[IWDW-AFGU#G3Z-5$9HQ&amp;U]FH/ODHMOZ(]EV?I,&amp;4J646?:)MI#LKH&gt;"4(N6^34ZF-^/\?`=O[O';U/N7I&amp;C-V`&amp;?`E\Y:O\=8&lt;(PA1#:/-3LP[B@QQU3&gt;'_H87#S2&gt;C)MAV%;-&amp;^CQ,,9\,D-(#[L_`A!GM$J\?B"NN!NL!W/+6T!BB^O*#F$R)X5&lt;KYPG&gt;Q).L:\Y^U_4N[1A-$`L\_I]&gt;$U5Z`/"1.W4OU5:+&gt;V42\[IS"!TT!Q4@1&lt;KZW"-&lt;'RL!!O"KKGV"VA2B0[LF:.FX*CI?.CF/P2O8?Q-J*$`==I&gt;5LW_$1-:P$-.P!VE'WPG,A&gt;T8CVSWEBGD"&lt;^8MYX=NQOJU#HYB##/1#&lt;`6GJ)*PS/I&gt;1ESY8=&gt;[JSRY(=%`6S#;?"XP2W`)Z#+X_(B99M?&gt;G&amp;$%L]=)2J_9^@6*_I4CO+Y_C&gt;=B:--R4F-9S?W)STH12VV=B#&gt;$+%47P]Z?PXLUQ+X!"M1T.Q!@RZIY(7V^C&amp;+/RN\%=$(1TU^+1WZL9!5FDHH;D'?&lt;]'L#]JBIZ[R[G9:0XLU#$0'68-3#]":TB%4[4P/CFCU&lt;'6F6]*MI1*&lt;%-JLR&lt;C$7;VEE1PI?2%5IK5L6[[A*6T&gt;0ET8)8D*:)Z`QK?6OB"&amp;`%QE1&amp;?B..'O$L[5GL?,UEY%['I6&gt;@K-;,:C.*BDDCG;*;C`@/&lt;2N%]`GH:,.!5+&lt;*.E,W,&amp;J88VKF&amp;R$U:TY_;P'-XAU&amp;=MGFTKAFJZ7+GZ9`N6JPV+U\\#W$`YS?14-&gt;VIIZ9#4,]W/\8U-7L:8;"23U\ZE'0)@Z;#.8"'6&gt;8S5FWU&amp;3EG,$P]L,AF+(!1GKVI@D]NGAG'MDY,GBU*.$N&lt;IJ'_BO/&gt;P6%\M:S1:0Z6-5\1?)+KCC%(8I/F?IK`6&gt;F*YK3G&amp;$+5FM"3K)([K9A&amp;O`?"RO]7EK[&amp;:D/6'%"CN&lt;VHN%56L'UZB6"YX8J:T)&gt;=D'"LSO&amp;,1V-@3P,#^7,=;Q4P6H_(3H3=#0[PN*L9UY^94_&gt;W;$XF[(6"L^)&gt;#JR'[KMR2V+&amp;',(=#7@JH&gt;$%OOB*&gt;&amp;(^&amp;Q6T&gt;&lt;\)=#FE[#!?UI_R&amp;HB)^T,$,A;0@/S'"V:D&amp;EO;U"J&gt;77UZ[]EL!"??0#I3I'PSZ"WT(V*..+R356STCT[\64-2@9)Z&amp;IFRDVZC7L?8I.L'1CE*UBKE9['D^F!U54M,43H[\&amp;&lt;.#8[+#;Y2Y`N.4;1Z_K$'&gt;E"3=K2F]$/2!&amp;W4,C2\.'F&amp;[7'&gt;HO44'T6H'.-SX'@+E$\J-C27$V1E84$&gt;&amp;B&gt;Y0D`4/.?JH=^=0*^$1&gt;I#PM5AWQ:*:FSL`IW73W#4&gt;?2NGJ*L.UVT=MCP\9B'_K=AWW&lt;ET3K.&lt;+FFF`I96E&amp;*EGGL\5T\6I)V&gt;=:=!:\EX8.P+J\;LP(5+;U/?4J0Y=C`)SQ@&amp;DAT"2_!0&gt;93P'--LXI*7+UQE[R%*:;#0?G$7O0.8-XBKQ&amp;W:BXMWV"VHOH/I%]D"JTV^AV;"PMZVC_DBYRB@B=&gt;ZIN4Q\)1^Z\:(_:&lt;UQXTT8A&lt;NG59T0@;BPF;V.K.U],5/G`&lt;BHG]=6'H,@MQP]]_T.@KPD).]`MT$@.`Y!&lt;`4&amp;^*]51?EO2+U(]0Q%]2Y^OM%V#)NK"O"B.1ZLM4MWT(7?#JJZ]/4?GJJJ_Q&gt;ABG:`LJ@)&lt;JJWP'U]]2S`4$G[;@\KT4T\(H.PW]_T_9@HJ?^/HHP2&gt;__OF^`N.0R$,^/#X4DX/\C:PY7^='?5L*^*8_5UA]@)MIK&gt;=/&amp;/L`YV&gt;8.:/]U)#RH&amp;(!)^58`3AU9D;&amp;@#0@)&amp;T%[R:PXPH]2@\_R-`*HXUHGJ/;M?M1*7P%,XX]QP]!_:UV`1!!!!1!!!"F!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!!T=!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!*-8!)!!!!!!!1!)!$$`````!!%!!!!!!(=!!!!&amp;!!Z!)1F4&gt;'^Q28:F&lt;H1!&amp;%"Q!"E!!1!!#6.U&lt;X"&amp;&gt;G6O&gt;!!?1(!!&amp;Q!!!!%!!1!!!_A!!1J"9X2P=E6W:7ZU!!!61!-!$V6Q:'&amp;U:5FO&gt;'6S&gt;G&amp;M&lt;!!;1&amp;!!!Q!#!!%!!QR#98.F,GRW9WRB=X-!!!%!"!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)!!!!R&amp;Q#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!$!!!!!!!!!!%!!!!#!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$7*\C&gt;!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!.9HO*U!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D!!!!ER=!A!!!!!!"!!A!-0````]!!1!!!!!!&gt;Q!!!!5!$E!B#6.U&lt;X"&amp;&gt;G6O&gt;!!51(!!'1!"!!!*5X2P=%6W:7ZU!"Z!=!!8!!!!!1!"!!!$[!!"#E&amp;D&gt;'^S28:F&lt;H1!!"6!!Q!068"E982F37ZU:8*W97RM!"J!5!!$!!)!!1!$$%*B=W5O&lt;(:D&lt;'&amp;T=Q!!!1!%!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G5!!!!:&amp;Q#!!!!!!!%!"1!$!!!"!!!!!!!-!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U91!!!)M8!)!!!!!!"1!/1#%*5X2P=%6W:7ZU!"2!=!!:!!%!!!F4&gt;'^Q28:F&lt;H1!(E"Q!"=!!!!"!!%!!!0I!!%+17.U&lt;X*&amp;&gt;G6O&gt;!!!&amp;5!$!!^6='2B&gt;'6*&lt;H2F=H:B&lt;'Q!'E"1!!-!!A!"!!--1G&amp;T:3ZM&gt;G.M98.T!!!"!!1!!!!!!!!!!0````]!!!!!!!!!!!1!#1!.!!!!"!!!!)5!!!!I!!!!!A!!"!!!!!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!41!!!)"?*S64UN/B%!5,'A:'"THIY//'AWO4&amp;T-RAOA2B.X2'0=CHT-**W"1!^R[57]BS@R$#\V"B9^')WO4)8O@P5?^;I!\+)@((3P66[=V_F=92Q5W)9"`/$WS7U":-G,.RDO3;TS=NG%&amp;QA-&lt;IIE5OHF8+6F(5G*7XS](LY]]S@BHE:6/J7VH.XXWG=MI[JS&gt;"%LC7E11M#EONA\EYO+)H[?_8L+,]J:47G@_B%M!CY3?HH8B7B,(-.'.\!B-PE!*XA3_:6;%GG?9;AX&gt;$G\+B:*BH8]3M2/G\O&gt;85-@NF#0#4MBP@5QQ.%`@*LAU7E/;DKYI`!+,CDF=`G'^GZB4..@M&amp;L]:&lt;Y\HM9SM)&amp;.PG+N;`"LNI1[E-?KQWK)%5./;'837';]%3&amp;U3"-\T@U*)^B/*!!!!'5!!1!#!!-!"!!!!%A!$Q1!!!!!$Q$9!.5!!!"2!!]%!!!!!!]!W!$6!!!!7A!0"!!!!!!0!.A!V1!!!'/!!)1!A!!!$Q$9!.5)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E"-!!!!&amp;*45E-.#A!$4&amp;:$1UR#6F=!!"4)!!!%-1!!!#!!!"3I!!!!!!!!!!!!!!!A!!!!.!!!"#A!!!!=4%F#4A!!!!!!!!&amp;A4&amp;:45A!!!!!!!!&amp;U5F242Q!!!!!!!!')1U.46!!!!!!!!!'=4%FW;1!!!!!!!!'Q1U^/5!!!!!!!!!(%6%UY-!!!!!!!!!(92%:%5Q!!!!!!!!(M4%FE=Q!!!!!!!!)!6EF$2!!!!!!!!!)5&gt;G6S=Q!!!!1!!!)I5U.45A!!!!!!!!+-2U.15A!!!!!!!!+A35.04A!!!!!!!!+U;7.M/!!!!!!!!!,)1V"$-A!!!!!!!!,=4%FG=!!!!!!!!!,Q2F")9A!!!!!!!!-%2F"421!!!!!!!!-96F"%5!!!!!!!!!-M4%FC:!!!!!!!!!.!1E2)9A!!!!!!!!.51E2421!!!!!!!!.I6EF55Q!!!!!!!!.]2&amp;2)5!!!!!!!!!/1466*2!!!!!!!!!/E3%F46!!!!!!!!!/Y6E.55!!!!!!!!!0-2F2"1A!!!!!!!!0A!!!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!$`````!!!!!!!!!-1!!!!!!!!!!0````]!!!!!!!!!W!!!!!!!!!!!`````Q!!!!!!!!$A!!!!!!!!!!$`````!!!!!!!!!1Q!!!!!!!!!!0````]!!!!!!!!"&amp;!!!!!!!!!!!`````Q!!!!!!!!&amp;!!!!!!!!!!!$`````!!!!!!!!!9Q!!!!!!!!!!0````]!!!!!!!!"H!!!!!!!!!!%`````Q!!!!!!!!.E!!!!!!!!!!@`````!!!!!!!!!X1!!!!!!!!!#0````]!!!!!!!!$B!!!!!!!!!!*`````Q!!!!!!!!/5!!!!!!!!!!L`````!!!!!!!!![1!!!!!!!!!!0````]!!!!!!!!$N!!!!!!!!!!!`````Q!!!!!!!!0-!!!!!!!!!!$`````!!!!!!!!!_!!!!!!!!!!!0````]!!!!!!!!%:!!!!!!!!!!!`````Q!!!!!!!!BI!!!!!!!!!!$`````!!!!!!!!#(!!!!!!!!!!!0````]!!!!!!!!)A!!!!!!!!!!!`````Q!!!!!!!!\]!!!!!!!!!!$`````!!!!!!!!$Q1!!!!!!!!!!0````]!!!!!!!!0$!!!!!!!!!!!`````Q!!!!!!!!]=!!!!!!!!!!$`````!!!!!!!!$Y1!!!!!!!!!!0````]!!!!!!!!0D!!!!!!!!!!!`````Q!!!!!!!",)!!!!!!!!!!$`````!!!!!!!!%N!!!!!!!!!!!0````]!!!!!!!!3W!!!!!!!!!!!`````Q!!!!!!!"-%!!!!!!!!!)$`````!!!!!!!!&amp;$Q!!!!!#%*B=W5O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>
<Name></Name>
<Val>!!!!!AJ#98.F,GRW&lt;'FC$%*B=W5O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!!!!5!!1!!!!!!!!%!!!!"!"2!5!!!$%*B=W5O&lt;(:D&lt;'&amp;T=Q!!!1!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!"``]!!!!"!!!!!!!"!1!!!!%!&amp;%"1!!!-1G&amp;T:3ZM&gt;G.M98.T!!!"!!!!!!!"`````A!!!!!!!!)617.U&lt;X)A2H*B&lt;76X&lt;X*L,GRW&lt;'FC$5&amp;D&gt;'^S,GRW9WRB=X.16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!!!!!!!!1!51&amp;!!!!R#98.F,GRW9WRB=X-!!!%!!!!!!!(````_!!!!!!!!!B6"9X2P=C"'=G&amp;N:8&gt;P=GMO&lt;(:M;7).17.U&lt;X)O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!!!!!1!!!!!!!1!!!!!%!!Z!)1F4&gt;'^Q28:F&lt;H1!&amp;%"Q!"E!!1!!#6.U&lt;X"&amp;&gt;G6O&gt;!!?1(!!&amp;Q!!!!%!!1!!!_A!!1J"9X2P=E6W:7ZU!!"6!0(7*\@1!!!!!QJ#98.F,GRW&lt;'FC$%*B=W5O&lt;(:D&lt;'&amp;T=QB#98.F,G.U&lt;!!M1&amp;!!!A!#!!%&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!$!!!!!P``````````!!!!!!!!!!!!!!!!!!)617.U&lt;X)A2H*B&lt;76X&lt;X*L,GRW&lt;'FC$5&amp;D&gt;'^S,GRW9WRB=X.16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!)!!!!!"1!/1#%*5X2P=%6W:7ZU!"2!=!!:!!%!!!F4&gt;'^Q28:F&lt;H1!(E"Q!"=!!!!"!!%!!!0I!!%+17.U&lt;X*&amp;&gt;G6O&gt;!!!&amp;5!$!!^6='2B&gt;'6*&lt;H2F=H:B&lt;'Q!6Q$RVC?YH1!!!!-+1G&amp;T:3ZM&gt;GRJ9AR#98.F,GRW9WRB=X-)1G&amp;T:3ZD&gt;'Q!,E"1!!-!!A!"!!-&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!%!!!!!Q!!!!!!!!!"`````Q!!!!!!!!!!`````Q!!!!!!!!)617.U&lt;X)A2H*B&lt;76X&lt;X*L,GRW&lt;'FC$5&amp;D&gt;'^S,GRW9WRB=X.16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!%!!!!-1G&amp;T:3ZM&gt;G.M98.T</Val>
</String>
</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"@!!!!!B6"9X2P=C"'=G&amp;N:8&gt;P=GMO&lt;(:M;7).17.U&lt;X)O&lt;(:D&lt;'&amp;T=V"53$!!!!!P!!!!"!=]&gt;GFM;7)_$E&amp;D&gt;'^S2H*B&lt;76X&lt;X*L"5&amp;D&gt;'^S$5&amp;D&gt;'^S,GRW9WRB=X-!!!!!</Property>
	<Item Name="Base.ctl" Type="Class Private Data" URL="Base.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="FrameworkOverride" Type="Folder">
		<Item Name="Actor Core.vi" Type="VI" URL="../FrameworkOverride/Actor Core.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%6!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!F&amp;5&amp;D&gt;'^S)%:S97VF&gt;W^S;SZM&gt;GRJ9AV"9X2P=CZM&gt;G.M98.T!!F"9X2P=C"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!#J!=!!?!!!:#E*B=W5O&lt;(:M;7)-1G&amp;T:3ZM&gt;G.M98.T!!&gt;#98.F)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!#1!!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">34079248</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Pre Launch Init.vi" Type="VI" URL="../FrameworkOverride/Pre Launch Init.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$T!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#R!=!!?!!!:#E*B=W5O&lt;(:M;7)-1G&amp;T:3ZM&gt;G.M98.T!!B#98.F)'^V&gt;!!!+E"Q!"Y!!"E+1G&amp;T:3ZM&gt;GRJ9AR#98.F,GRW9WRB=X-!"U*B=W5A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!1!"!!%!!9$!!"Y!!!*!!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*)!!!!!!1!(!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">277618704</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Stop Core.vi" Type="VI" URL="../FrameworkOverride/Stop Core.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$.!!!!"1!%!!!!,%"Q!"Y!!"E+1G&amp;T:3ZM&gt;GRJ9AR#98.F,GRW9WRB=X-!#%*B=W5A&lt;X6U!!!81!-!%':J&lt;G&amp;M)'6S=G^S)'.P:'5!!#J!=!!?!!!:#E*B=W5O&lt;(:M;7)-1G&amp;T:3ZM&gt;G.M98.T!!&gt;#98.F)'FO!&amp;1!]!!-!!!!!!!!!!%!!!!!!!!!!!!!!!!!!A!$!Q!!?!!!!!!!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"!!!!#3!!!!!!%!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">277619216</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
	</Item>
	<Item Name="Properties" Type="Folder">
		<Item Name="ActorEvent" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">ActorEvent</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">ActorEvent</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read ActorEvent.vi" Type="VI" URL="../Properties/Read ActorEvent.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;A!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1F4&gt;'^Q28:F&lt;H1!&amp;%"Q!"E!!1!&amp;#6.U&lt;X"&amp;&gt;G6O&gt;!!?1(!!&amp;Q!!!!%!!1!!!_A!"AJ"9X2P=E6W:7ZU!!!M1(!!(A!!'1J#98.F,GRW&lt;'FC$%*B=W5O&lt;(:D&lt;'&amp;T=Q!)1G&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+E"Q!"Y!!"E+1G&amp;T:3ZM&gt;GRJ9AR#98.F,GRW9WRB=X-!"U*B=W5A;7Y!91$Q!!Q!!Q!%!!=!#!!%!!1!"!!%!!E!"!!%!!I#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write ActorEvent.vi" Type="VI" URL="../Properties/Write ActorEvent.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;A!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#R!=!!?!!!:#E*B=W5O&lt;(:M;7)-1G&amp;T:3ZM&gt;G.M98.T!!B#98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!/1#%*5X2P=%6W:7ZU!"2!=!!:!!%!"QF4&gt;'^Q28:F&lt;H1!(E"Q!"=!!!!"!!%!!!0I!!A+17.U&lt;X*&amp;&gt;G6O&gt;!!!+E"Q!"Y!!"E+1G&amp;T:3ZM&gt;GRJ9AR#98.F,GRW9WRB=X-!"U*B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!*!!I#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
			</Item>
		</Item>
		<Item Name="StopEvent" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">StopEvent</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">StopEvent</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read StopEvent.vi" Type="VI" URL="../Properties/Read StopEvent.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;#!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1F4&gt;'^Q28:F&lt;H1!&amp;%"Q!"E!!1!&amp;#6.U&lt;X"&amp;&gt;G6O&gt;!!M1(!!(A!!'1J#98.F,GRW&lt;'FC$%*B=W5O&lt;(:D&lt;'&amp;T=Q!)1G&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+E"Q!"Y!!"E+1G&amp;T:3ZM&gt;GRJ9AR#98.F,GRW9WRB=X-!"U*B=W5A;7Y!91$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!%!!E#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write StopEvent.vi" Type="VI" URL="../Properties/Write StopEvent.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;#!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#R!=!!?!!!:#E*B=W5O&lt;(:M;7)-1G&amp;T:3ZM&gt;G.M98.T!!B#98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!/1#%*5X2P=%6W:7ZU!"2!=!!:!!%!"QF4&gt;'^Q28:F&lt;H1!+E"Q!"Y!!"E+1G&amp;T:3ZM&gt;GRJ9AR#98.F,GRW9WRB=X-!"U*B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!)!!E#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
			</Item>
		</Item>
		<Item Name="UpdateIntervall" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">UpdateIntervall</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">UpdateIntervall</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read UpdateIntervall.vi" Type="VI" URL="../Properties/Read UpdateIntervall.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%V!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!!Q!068"E982F37ZU:8*W97RM!#R!=!!?!!!:#E*B=W5O&lt;(:M;7)-1G&amp;T:3ZM&gt;G.M98.T!!B#98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!K1(!!(A!!'1J#98.F,GRW&lt;'FC$%*B=W5O&lt;(:D&lt;'&amp;T=Q!(1G&amp;T:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write UpdateIntervall.vi" Type="VI" URL="../Properties/Write UpdateIntervall.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%V!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#R!=!!?!!!:#E*B=W5O&lt;(:M;7)-1G&amp;T:3ZM&gt;G.M98.T!!B#98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!61!-!$V6Q:'&amp;U:5FO&gt;'6S&gt;G&amp;M&lt;!!K1(!!(A!!'1J#98.F,GRW&lt;'FC$%*B=W5O&lt;(:D&lt;'&amp;T=Q!(1G&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
			</Item>
		</Item>
	</Item>
</LVClass>
